﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace MobileAppAndroid
{
    public class Utilities
    {

        public static string GetCheckedStatus(int EmployeeId, int MeetingId)
        {
            string CheckedStatus = "False";
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var Result = ent.MeetingEmployeeDetails.SingleOrDefault(x => x.EmployeeId == EmployeeId && x.MeetingId == MeetingId);
                if (Result != null)
                {
                    CheckedStatus = "True";
                }
                else
                {
                    CheckedStatus = "False";
                }
                return CheckedStatus;
            }
        }

        public static SelectList GetDepartment()
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var obj = ent.DepartmentDetails.ToList();
                return new SelectList(ent.DepartmentDetails.ToList(), "DepartmentId", "Name");
            }
        }


        public static SelectList GetDepartment(int OrganizationId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var obj = ent.DepartmentDetails.Where(x => x.OrganizationId == OrganizationId).ToList();
                return new SelectList(obj.ToList(), "DepartmentId", "Name");
            }
        }

        public static bool IsUserAlreadyInMeeting(int MeetingId, int EmployeeId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                int Count = ent.MeetingEmployeeDetails.Where(x => x.MeetingId == MeetingId && x.EmployeeId == EmployeeId).ToList().Count;
                if (Count > 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }


        public static int TotalAssignEmployeeInMeeting(int MeetingId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                int Total = ent.MeetingEmployeeDetails.Where(x => x.MeetingId == MeetingId).ToList().Count;
                return Total;


            }
        }


        public static int GetUserID(string username)
        {
            int i = WebSecurity.GetUserId(username);
            return i;
        }

        public static int GetCurrentLoginUserId()
        {
            var UserContext = new UsersContext();
            var userId = WebSecurity.GetUserId(HttpContext.Current.User.Identity.Name);//user profile user id
            return userId;
        }

        public static int GetEmployeeIdFromUserId()
        {
            using (UsersContext ent = new UsersContext())
            {
                int id = GetCurrentLoginUserId();
                var obj = ent.UserProfiles.Where(x => x.UserId == id).SingleOrDefault();
                if (obj != null)
                    return Convert.ToInt32(obj.EmployeeId);
                else
                    return 0;
            }
        }

        public static int GetCurrentEmployeeOrganizationId(int EmployeeId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var Result = ent.EmployeeDetails.SingleOrDefault(x => x.EmployeeId == EmployeeId);
                if (Result != null)
                {
                    return Result.OrganizationId;
                }
                else
                    return 0;
            }
        }

        public static IEnumerable<SelectListItem> MeetingAttendStatus()
        {
            return new SelectList(new[]
            {
                new {Id="1",Value="Yes"},
                new {Id="2",Value="No"},
            }, "Id", "Value");

        }



    }
}