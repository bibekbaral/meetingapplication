﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileAppAndroid.Models;
using WebMatrix.WebData;
using System.Web.Security;



namespace MobileAppAndroid.Providers
{
    public class EmployeeRecordsProviders
    {

        public List<EmployeeDetailsModel> GetList(int OrganizationId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<EmployeeDetailsModel> ReturnList = ent.EmployeeDetails.Where(x => x.OrganizationId == OrganizationId).Select(x => new EmployeeDetailsModel()
                {
                    Email = x.Email,
                    Name = x.Name,
                    OrganizationId = x.OrganizationId,
                    UserType = x.UserType,
                    DepartmentId = x.DepartmentId,
                    EmployeeId = x.EmployeeId

                }).ToList();

                return ReturnList;
            }

        }

        public List<EmployeeDetailsModel> GetList()
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<EmployeeDetailsModel> ReturnList = ent.EmployeeDetails.Select(x => new EmployeeDetailsModel()
                {
                    Email = x.Email,
                    Name = x.Name,
                    OrganizationId = x.OrganizationId,
                    UserType = x.UserType,
                    DepartmentId = x.DepartmentId,
                    EmployeeId = x.EmployeeId


                }).ToList();

                return ReturnList;
            }
        }

        public bool InsertEmployee(EmployeeDetailsModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var ObjToInesert = new EmployeeDetail()
                {
                    Name = model.Name,
                    Email = model.Email,
                    DepartmentId = model.DepartmentId,
                    OrganizationId = model.OrganizationId,
                    UserType = model.UserType

                };

                ent.EmployeeDetails.Add(ObjToInesert);
                model.UserPassword = model.UserPassword;



                int i = ent.SaveChanges();
                if (i > 0)
                {
                    WebSecurity.CreateUserAndAccount(model.Email, model.UserPassword, new { UserType = model.UserType, OrganizationId = model.OrganizationId, EmployeeId = ObjToInesert.EmployeeId, UserStatus = 1 });
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsAlreadyemail(string email)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var result = ent.EmployeeDetails.Where(x => x.Email.Contains(email)).ToList();
                if (result.Count > 0)
                {
                    return false;
                }
                else
                    return true;
            }
        }

        public bool EditEmployeeName(EmployeeDetailsModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var EditObj = ent.EmployeeDetails.SingleOrDefault(x => x.EmployeeId == model.EmployeeId);
                if (EditObj != null)
                {
                    EditObj.Name = model.Name;
                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool EditEmployee(EmployeeDetailsModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var EditObj = ent.EmployeeDetails.SingleOrDefault(x => x.EmployeeId == model.EmployeeId);
                if (EditObj != null)
                {
                    model.Name = model.Name;
                    model.Email = model.Email;
                    model.DepartmentId = model.DepartmentId;
                    model.OrganizationId = model.OrganizationId;
                    model.UserType = model.UserType;

                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool DeleteEmployee(int EmployeeId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                //first delete it from meeting
                ent.MeetingEmployeeDetails.RemoveRange(ent.MeetingEmployeeDetails.Where(x => x.EmployeeId == EmployeeId));
                //delete from employee records
                ent.EmployeeDetails.RemoveRange(ent.EmployeeDetails.Where(x => x.EmployeeId == EmployeeId));


                //DeleteUserRoleAndId(EmployeeId);
                int i = ent.SaveChanges();
                if (i > 0)
                {
                    DeleteUserRoleAndId(EmployeeId);
                    return true;

                }
                else
                    return false;
            }
        }


        public void DeleteUserRoleAndId(int UserId)
        {
            var UserContext = new UsersContext();
            var user = UserContext.UserProfiles.SingleOrDefault(u => u.EmployeeId == UserId);
            string UserName = user.UserName;

            if (WebMatrix.WebData.WebSecurity.UserExists(UserName))
            {

                ((SimpleMembershipProvider)System.Web.Security.Membership.Provider).DeleteAccount(UserName);
                ((SimpleMembershipProvider)System.Web.Security.Membership.Provider).DeleteUser(UserName, true);


            }



        }

        //Update Device Info
        public void InsertEmployeeDeviceInfo(int EmployeeId, string DeviceId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var Result = ent.EmployeeDetails.SingleOrDefault(x => x.EmployeeId == EmployeeId);
                if (Result != null)
                {
                    Result.DeviceId = DeviceId;
                    ent.SaveChanges();
                }
            }
        }

        public List<EmployeeCheckListVM> EmployeeCheckList(int OrganizationId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<EmployeeCheckListVM> ReturnList = ent.EmployeeDetails.Where(x => x.OrganizationId == OrganizationId).Select(x => new EmployeeCheckListVM()
                {
                    EmployeeId = x.EmployeeId,
                    EmployeeName = x.Name,
                    IsChecked = false

                }).ToList();
                return ReturnList;
            }
        }



        public bool AssignEmployeeInMeetingFromAdmin(EmployeeDetailsModel model)
        {
            bool RetrunStatus = false;
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                ent.MeetingEmployeeDetails.RemoveRange(ent.MeetingEmployeeDetails.Where(x => x.MeetingId == model.ObjEmployeeMeetingDetailViewModel.MeetiingDetailsId));

                if (model.EmployeeCheckListVMList.Count > 0)
                {
                    foreach (var item in model.EmployeeCheckListVMList)
                    {
                        if (item.IsChecked)
                        {
                            var ObjToInsert = new MeetingEmployeeDetails()
                            {

                                EmployeeId = item.EmployeeId,
                                MeetingId = model.ObjEmployeeMeetingDetailViewModel.MeetiingDetailsId,
                                Feedback = "",
                                Remarks = "",
                                Status = 1
                            };
                            ent.MeetingEmployeeDetails.Add(ObjToInsert);
                            int i = ent.SaveChanges();
                            if (i > 0)
                            {

                                RetrunStatus = true;
                            }
                            else
                            {

                                RetrunStatus = false;

                            }
                        }

                    }
                    return RetrunStatus;
                }
                else
                {
                    return false;
                }
            }
        }


        public List<EmployeeCheckListVM> EmployeeCheckListIfExist(int MeetingId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<EmployeeCheckListVM> list = new List<EmployeeCheckListVM>();
                var Result = EmployeeCheckList(1);
                foreach (var item in Result)
                {
                    EmployeeCheckListVM ChkList = new EmployeeCheckListVM();
                    ChkList.EmployeeId = item.EmployeeId;
                    ChkList.EmployeeName = item.EmployeeName;

                    if (Utilities.IsUserAlreadyInMeeting(MeetingId,Convert.ToInt32(item.EmployeeId)))
                    {
                        ChkList.IsChecked = true;
                    }
                    else
                    {
                        ChkList.IsChecked = false;
                    }
                    list.Add(ChkList);

                }

                return list;
            }
        }
    }
}