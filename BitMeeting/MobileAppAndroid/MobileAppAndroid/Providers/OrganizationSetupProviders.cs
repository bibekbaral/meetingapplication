﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileAppAndroid.Models;

namespace MobileAppAndroid.Providers
{
    public class OrganizationSetupProviders
    {

        public List<OrganizationDetailsModel> GetList()
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<OrganizationDetailsModel> ReturnList = ent.OrganizationDetails.Select(x => new OrganizationDetailsModel()
                {
                    OrganizationName=x.OrganizationName,
                    Id=x.Id,
                    EmailId=x.EmailId,
                    Address=x.Address,
                    Mobile=x.Mobile,
                    PhNumber=x.PhNumber,
                    Status=x.Status

                }).ToList();
                return ReturnList;

            }
        }

        public bool InsertOrganization(OrganizationDetailsModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var ObjtoInsert = new OrganizationDetails()
                {
                    OrganizationName=model.OrganizationName,
                    EmailId=model.EmailId,
                    PhNumber=model.PhNumber,
                    Address=model.Address,
                    Status=1,
                    Mobile=model.Mobile,
                    CreatedDate=DateTime.Now

                };
                ent.OrganizationDetails.Add(ObjtoInsert);
                int i = ent.SaveChanges();
                if (i > 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public bool Edit(OrganizationDetailsModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                //Edit only first stage
                var Edit = ent.OrganizationDetails.Where(x => x.Id == model.Id && x.Status==1).SingleOrDefault();
                if (Edit != null)
                {
                    Edit.OrganizationName = model.OrganizationName;
                    Edit.EmailId = model.EmailId;
                    Edit.PhNumber = model.PhNumber;
                    Edit.Mobile = model.Mobile;
                    Edit.Address = model.Address;
                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        public bool ApproveOrRejectStatus(int OrganizationId,int AppOrRejStatus)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var ApproveOrReject = ent.OrganizationDetails.SingleOrDefault(x => x.Id == OrganizationId);
                if (ApproveOrReject != null)
                {
                    ApproveOrReject.Status = AppOrRejStatus;
                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        public bool DeleteOrganizationId(int OrganizationId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var Delete = ent.OrganizationDetails.SingleOrDefault(x => x.Id == OrganizationId);
                if (Delete != null)
                {
                    Delete.Status = 0;
                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
        }
    }
}