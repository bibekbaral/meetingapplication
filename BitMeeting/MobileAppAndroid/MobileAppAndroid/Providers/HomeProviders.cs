﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileAppAndroid.Models;

namespace MobileAppAndroid.Providers
{
    public class HomeProviders
    {

        public List<EmployeeDetailsVMHome> GetEmployeeList()
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<EmployeeDetailsVMHome> ReturnList = ent.EmployeeDetails.Select(x => new EmployeeDetailsVMHome()
                {
                    Email=x.Email,
                    DeviceId=x.DeviceId,
                    Name=x.Name,
                    EmployeeId=x.EmployeeId

                }).ToList();

                return ReturnList;
            }
        }

        public List<MeetingDetailsVMHome> GetMeetingDetailsForHome()
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<MeetingDetailsVMHome> ReturnList = ent.MeetingDetails.Select(x => new MeetingDetailsVMHome()
                {
                    MeetingId=x.MeetiingDetailsId,
                    MeetingTitle=x.MeetingTitle,
                    MeetingDate=x.MeetingDate
                }).ToList();
                return ReturnList;
            }

        }
    }
}