﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileAppAndroid.Models;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;
using System.Text;


namespace MobileAppAndroid.Providers
{
    public class MeetingDetailsProviders
    {

        public List<MeetingDetailsMainModel> GetMeetingList(int organizationID)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<MeetingDetailsMainModel> ReturnList = ent.MeetingDetails.Where(x => x.OrganizationId == organizationID).Select(x => new MeetingDetailsMainModel()
                {
                    MeetiingDetailsId = x.MeetiingDetailsId,
                    OrganizationId = x.OrganizationId,
                    Venue = x.Venue,
                    MeetingDate = x.MeetingDate,
                    MeetingTime = (TimeSpan)x.Time,
                    MeetingTitle = x.MeetingTitle,
                    Agenda = x.Agenda
                }).ToList();

                return ReturnList;
            }
        }
        public List<MeetingDetailsMainModel> GetMeetingList()
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<MeetingDetailsMainModel> ReturnList = ent.MeetingDetails.Select(x => new MeetingDetailsMainModel()
                {
                    MeetiingDetailsId = x.MeetiingDetailsId,
                    OrganizationId = x.OrganizationId,
                    Venue = x.Venue,
                    MeetingDate = x.MeetingDate,
                    MeetingTime = (TimeSpan)x.Time,
                    MeetingTitle = x.MeetingTitle,
                    Agenda = x.Agenda
                }).ToList();

                return ReturnList;
            }
        }

        public int InsertMeetingDetails(MeetingDetailsMainModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var ObjToInsert = new MeetingDetails()
                {
                    MeetingTitle = model.MeetingTitle,
                    Venue = model.Venue,
                    Agenda = model.Agenda,
                    CreatedBy = model.CreatedBy,
                    CreatedDate = DateTime.Now,
                    MeetingDate = model.MeetingDate,
                    Time = model.MeetingTime,
                    OrganizationId = model.OrganizationId
                };
                ent.MeetingDetails.Add(ObjToInsert);
                int i = ent.SaveChanges();
                if (i > 0)
                {
                    return ObjToInsert.MeetiingDetailsId;
                }
                else
                    return 0;
            }
        }

        public bool InsertMeetingDetailsAdmin(MeetingDetailsMainModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var ObjToInsert = new MeetingDetails()
                {
                    MeetingTitle = model.MeetingTitle,
                    Venue = model.Venue,
                    Agenda = model.Agenda,
                    CreatedBy = model.CreatedBy,
                    CreatedDate = DateTime.Now,
                    MeetingDate = model.MeetingDate,
                    Time = model.MeetingTime,
                    OrganizationId = model.OrganizationId
                };
                ent.MeetingDetails.Add(ObjToInsert);
                int i = ent.SaveChanges();
                if (i > 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public bool EditMeetingDetails(MeetingDetailsMainModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var ObjToEdit = ent.MeetingDetails.SingleOrDefault(x => x.MeetiingDetailsId == model.MeetiingDetailsId);

                if (ObjToEdit != null)
                {
                    ObjToEdit.MeetingTitle = model.MeetingTitle;
                    ObjToEdit.MeetingDate = model.MeetingDate;
                    ObjToEdit.Time = model.MeetingTime;
                    ObjToEdit.Venue = model.Venue;
                    ObjToEdit.Agenda = model.Agenda;
                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        public bool InsertEmployeeInMeeting(MeetingDetailsMainModel model, string[] EmployeeIds)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                //First Delete All Employee From Meeting Employee Details table
                int MeetingId = model.ObjMeetingEmployeeDetailsViewModel.MeetingId;
                ent.MeetingEmployeeDetails.RemoveRange(ent.MeetingEmployeeDetails.Where(x => x.MeetingId == MeetingId));
                ent.SaveChanges();


                if (EmployeeIds.Length > 0)
                {
                    bool ReturnStmt = false;
                    foreach (var item in EmployeeIds)
                    {
                        int EmployeeIdInt = Convert.ToInt32(item);
                        int checkuser = ent.MeetingEmployeeDetails.Where(x => x.MeetingId == model.ObjMeetingEmployeeDetailsViewModel.MeetingId && x.EmployeeId == EmployeeIdInt).ToList().Count();
                        if (checkuser <= 0)
                        {
                            var ObjToInsert = new MeetingEmployeeDetails()
                            {

                                EmployeeId = EmployeeIdInt,
                                MeetingId = model.ObjMeetingEmployeeDetailsViewModel.MeetingId,
                                Feedback = model.ObjMeetingEmployeeDetailsViewModel.Feedback,
                                Remarks = model.ObjMeetingEmployeeDetailsViewModel.Remarks,
                                Status = model.ObjMeetingEmployeeDetailsViewModel.Status
                            };
                            ent.MeetingEmployeeDetails.Add(ObjToInsert);
                            int i = ent.SaveChanges();
                            if (i > 0)
                            {

                                ReturnStmt = true;
                            }
                            else
                            {

                                return false;

                            }
                        }

                    }
                    return ReturnStmt;

                }
                else
                {
                    return false;
                }

            }
        }


        public List<EmployeeFromGroupIdViewModel> GetEmployeeListFromGroupIds(string[] GroupIds)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {

                List<EmployeeFromGroupIdViewModel> list = new List<EmployeeFromGroupIdViewModel>();
                if (GroupIds.Length > 0)
                {
                    foreach (var item in GroupIds)
                    {
                        int GroupIdInt = Convert.ToInt32(item);
                        var result = ent.EmployeeDetails.Where(x => x.DepartmentId == GroupIdInt).ToList();
                        foreach (var item1 in result)
                        {
                            EmployeeFromGroupIdViewModel data = new EmployeeFromGroupIdViewModel()
                            {
                                EmployeeIds = item1.EmployeeId
                            };
                            list.Add(data);
                        }

                    }
                }
                return list;
            }
        }


        public bool InsertEmployeeGroupInMeeting(MeetingDetailsMainModel model)
        {
            bool ReturnStmt = false;
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                int MeetingId = model.ObjMeetingEmployeeDetailsViewModel.MeetingId;
                ent.MeetingEmployeeDetails.RemoveRange(ent.MeetingEmployeeDetails.Where(x => x.MeetingId == MeetingId));
                ent.SaveChanges();

                if (model.EmployeeFromGroupIdViewModelList.Count > 0)
                {
                    foreach (var item1 in model.EmployeeFromGroupIdViewModelList)
                    {
                        //check if user already inserted for same meeting


                        int EmployeeIdInt = Convert.ToInt32(item1.EmployeeIds);
                        int checkuser = ent.MeetingEmployeeDetails.Where(x => x.MeetingId == model.ObjMeetingEmployeeDetailsViewModel.MeetingId && x.EmployeeId == EmployeeIdInt).ToList().Count();
                        if (checkuser <= 0)
                        {
                            var ObjToInsert = new MeetingEmployeeDetails()
                            {

                                EmployeeId = item1.EmployeeIds,
                                MeetingId = model.ObjMeetingEmployeeDetailsViewModel.MeetingId,
                                Feedback = model.ObjMeetingEmployeeDetailsViewModel.Feedback,
                                Remarks = model.ObjMeetingEmployeeDetailsViewModel.Remarks,
                                Status = model.ObjMeetingEmployeeDetailsViewModel.Status
                            };
                            ent.MeetingEmployeeDetails.Add(ObjToInsert);
                            int i = ent.SaveChanges();
                            if (i > 0)
                            {
                                ReturnStmt = true;
                            }
                            else
                            {

                                ReturnStmt = false;
                            }
                        }



                    }
                    return ReturnStmt;
                }
                else
                    return false;


            }

        }


        public List<MeetingEmployeeDetailsViewModel> GetMeetingEmployeeList(int MeetingId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<MeetingEmployeeDetailsViewModel> ReturnList = ent.MeetingEmployeeDetails.Where(x => x.MeetingId == MeetingId).Select(x => new MeetingEmployeeDetailsViewModel()
                {
                    MeetingEmpId = x.MeetingEmpId,
                    EmployeeId = x.EmployeeId,
                    Status = x.Status,
                    Remarks = x.Remarks,
                    Feedback = x.Feedback,
                    MeetingId = x.MeetingId

                }).ToList();

                return ReturnList;
            }
        }

        public bool DeleteMeetingDetails(MeetingEmployeeDetailsViewModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                //First Delete from Employoee

                //Delete Meeting Details

                var DeleteMeetingDetails = ent.MeetingDetails.SingleOrDefault(x => x.MeetiingDetailsId == model.MeetingId);
                if (DeleteMeetingDetails != null)
                {
                    ent.MeetingEmployeeDetails.RemoveRange(ent.MeetingEmployeeDetails.Where(x => x.MeetingId == model.MeetingId));
                    ent.MeetingDetails.Remove(DeleteMeetingDetails);
                    ent.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }


        }


        public bool DeleteEmployeeFromMeeting(MeetingEmployeeDetailsViewModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var ItemToRemove = ent.MeetingEmployeeDetails.SingleOrDefault(x => x.MeetingEmpId == model.MeetingEmpId);
                if (ItemToRemove != null)
                {
                    ent.MeetingEmployeeDetails.Remove(ItemToRemove);
                    ent.SaveChanges();
                    return true;
                }
                else
                    return false;
            }


        }

        public IEnumerable<EmployeeMeetingDetailViewModel> GetEmployeeMeetingDetails(int OrganizationId, int EmployeeId)
        {


            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                IEnumerable<EmployeeMeetingDetailViewModel> RetrunList = new List<EmployeeMeetingDetailViewModel>();

                RetrunList = (from s in ent.MeetingDetails
                              join sa in ent.MeetingEmployeeDetails on s.MeetiingDetailsId equals sa.MeetingId
                              where sa.EmployeeId == EmployeeId && s.OrganizationId == OrganizationId
                              select new EmployeeMeetingDetailViewModel()
                             {
                                 MeetiingDetailsId = s.MeetiingDetailsId,
                                 MeetingDate = s.MeetingDate,
                                 MeetingTime = (TimeSpan)s.Time,
                                 Venue = s.Venue,
                                 OrganizationId = s.OrganizationId,
                                 Agenda = s.Agenda,
                                 MeetingTitle = s.MeetingTitle,
                                 EmployeeId = sa.EmployeeId,
                                 FeedBack = sa.Feedback,
                                 Remarks = sa.Remarks,
                                 Status = sa.Status
                             });

                return RetrunList.ToList();

            }

        }


        public IEnumerable<EmployeeMeetingDetailViewModel> GetEmployeeListFromMeetingId(int MeetingId)
        {

            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                IEnumerable<EmployeeMeetingDetailViewModel> RetrunList = new List<EmployeeMeetingDetailViewModel>();

                RetrunList = (from s in ent.MeetingDetails
                              join sa in ent.MeetingEmployeeDetails on s.MeetiingDetailsId equals sa.MeetingId
                              join saa in ent.EmployeeDetails on sa.EmployeeId equals saa.EmployeeId
                              where s.MeetiingDetailsId == MeetingId
                              select new EmployeeMeetingDetailViewModel()
                              {
                                  MeetiingDetailsId = s.MeetiingDetailsId,
                                  MeetingDate = s.MeetingDate,
                                  MeetingTime = (TimeSpan)s.Time,
                                  Venue = s.Venue,
                                  OrganizationId = s.OrganizationId,
                                  Agenda = saa.Name,
                                  MeetingTitle = s.MeetingTitle,
                                  EmployeeId = sa.EmployeeId,
                                  FeedBack = sa.Feedback,
                                  Remarks = sa.Remarks,



                              });

                return RetrunList.ToList();

            }

        }

        public bool UpdateYesNoStaus(MeetingDetailsMainModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                //first get meeting details id

                var Result = ent.MeetingEmployeeDetails.SingleOrDefault(x => x.MeetingId == model.ObjMeetingEmployeeDetailsViewModel.MeetingId && x.EmployeeId == model.ObjMeetingEmployeeDetailsViewModel.EmployeeId);
                if (Result != null)
                {

                    //model.ObjMeetingEmployeeDetailsViewModel.Remarks = "";
                    model.ObjMeetingEmployeeDetailsViewModel.Feedback = "";

                    Result.Status = model.ObjMeetingEmployeeDetailsViewModel.Status;
                    Result.Remarks = model.ObjMeetingEmployeeDetailsViewModel.Remarks;
                    Result.Feedback = model.ObjMeetingEmployeeDetailsViewModel.Feedback;

                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }


                }
                else
                    return false;
            }
        }
        public bool InsertFeedback(MeetingDetailsMainModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var Result = ent.MeetingEmployeeDetails.SingleOrDefault(x => x.MeetingId == model.ObjMeetingEmployeeDetailsViewModel.MeetingId && x.EmployeeId == model.ObjMeetingEmployeeDetailsViewModel.EmployeeId);
                if (Result != null)
                {
                    Result.Feedback = model.ObjMeetingEmployeeDetailsViewModel.Feedback;
                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }


                }
                else
                    return false;
            }
        }

        public string GetYesNoStaus(int StatusId)
        {
            if (StatusId == 1)
            {
                return "No Action";
            }
            else if (StatusId == 2)
            {
                return "Yes";
            }
            else
            {
                return "No";
            }
        }

        public IEnumerable<MeetingEmployeeDetailsForAdminVM> GetMeetingDetailsforAdmin(int MeetingId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                IEnumerable<MeetingEmployeeDetailsForAdminVM> RetrunList = new List<MeetingEmployeeDetailsForAdminVM>();

                RetrunList = (from s in ent.EmployeeDetails
                              join sa in ent.MeetingEmployeeDetails on s.EmployeeId equals sa.EmployeeId
                              where sa.MeetingId == MeetingId
                              select new MeetingEmployeeDetailsForAdminVM()
                              {
                                  EmployeeName = s.Name,
                                  YesNoStatus = sa.Status.ToString(),
                                  Remarks = sa.Remarks,
                                  FeedBack = sa.Feedback

                              });

                return RetrunList.ToList();
            }


        }
        public int GetTotalAttendeeNumber(int MeetingId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var Total = ent.MeetingEmployeeDetails.Where(x => x.MeetingId == MeetingId && x.Status == 2).ToList();
                int TotalCount = Total.Count;

                return TotalCount;
            }
        }

        public bool GetEmployeeDetailsEditByMeetingId(int MeetingId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {


            }

            return false;
        }

        public int GetOrganizationIdFromMeetingId(int MeetingId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var Result = ent.MeetingDetails.SingleOrDefault(x => x.MeetiingDetailsId == MeetingId);
                if (Result != null)
                {
                    return Result.OrganizationId;
                }
                else
                    return 0;
            }
        }


        public void SendNotificationToUser(int MeetingId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var EmployeeList = ent.MeetingEmployeeDetails.Where(x => x.MeetingId == MeetingId).ToList();
                string MeetingTitle = ent.MeetingDetails.Where(x => x.MeetiingDetailsId == MeetingId).FirstOrDefault().MeetingTitle;

                foreach (var item in EmployeeList)
                {
                    string DeviceId = ent.EmployeeDetails.Where(x => x.EmployeeId == item.EmployeeId).FirstOrDefault().DeviceId;
                    if (DeviceId != null)
                    {
                        SendNotificationControllerMM(DeviceId, MeetingTitle);
                    }

                }

            }
        }

        public AndroidFCMPushNotificationStatus SendNotificationControllerMM(string DeviceId, string NoticeTitle)
        {
            AndroidFCMPushNotificationStatus result = new AndroidFCMPushNotificationStatus();
            string serverApiKey = "AAAAmj_63s8:APA91bFLo3dHQwbrI_KNtRjvFr34__Mu8iByqYPuz-Edzc12cFWUzeH_xauk9wiDDPsYeJcHnmkLTdZys8ymPm9R_-EEl7sa-KepFqfUsEHj6jp3TE8rDMY4rMmDEczcfvNm8am86rOyQX-bWixXv_VLn4VXNjWEQw";
            string senderId = "662498369231";
            string deviceId = DeviceId;

            try
            {
                result.Successful = false;
                result.Error = null;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";

                //tRequest.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", serverApiKey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                //string postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message=" + value + "&data.time=" + System.DateTime.Now.ToString() + "&registration_id=" + deviceId + "";

                //Byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        time_to_live=108,
                        delay_while_idle=1,
                        collapse_key="score_update",
                        body = NoticeTitle,
                        title = "Meeting Application",
                        icon = "myicon",
                        sound = "default"                        
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);



                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }

            return result;
        }


        public class AndroidFCMPushNotificationStatus
        {
            public bool Successful
            {
                get;
                set;
            }

            public string Response
            {
                get;
                set;
            }
            public Exception Error
            {
                get;
                set;
            }
        }

        public int CheckIsAlreadyEmployeeAssignOrNot(int MeetingId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                int count = ent.MeetingEmployeeDetails.Where(x => x.MeetingId == MeetingId).ToList().Count();
                return count;
            }
        }

       



    }
}