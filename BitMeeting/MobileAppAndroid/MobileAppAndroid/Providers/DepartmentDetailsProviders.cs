﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileAppAndroid.Models;

namespace MobileAppAndroid.Providers
{
    public class DepartmentDetailsProviders
    {

        public List<DepartmentDetailsModel> GetDepartmentList(int OrganizationId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                List<DepartmentDetailsModel> ReturnList = ent.DepartmentDetails.Where(x => x.OrganizationId == OrganizationId).Select(x => new DepartmentDetailsModel()
                {
                    DepartmentId = x.DepartmentId,
                    Name = x.Name,
                    OrganizationId = x.OrganizationId

                }).ToList();

                return ReturnList;
            }
        }

        public List<DepartmentDetailsModel> GetDepartmentList()
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                
                List<DepartmentDetailsModel> ReturnList = ent.DepartmentDetails.Select(x => new DepartmentDetailsModel()
                {
                    DepartmentId = x.DepartmentId,
                    Name = x.Name,
                    OrganizationId = x.OrganizationId

                }).ToList();

                return ReturnList;
            }
        }

        public bool InsertDepartment(DepartmentDetailsModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var ObjtoInsert = new DepartmentDetails()
                {
                    Name = model.Name,
                    OrganizationId = model.OrganizationId,
                    Status=2
                };
                ent.DepartmentDetails.Add(ObjtoInsert);
                int i = ent.SaveChanges();
                if (i > 0)
                {
                    return true;
                }
                else
                    return false;

            }

        }

        public bool EditDeaprtment(DepartmentDetailsModel model)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var ResultToEdit = ent.DepartmentDetails.SingleOrDefault(x => x.DepartmentId == model.DepartmentId);
                if (ResultToEdit != null)
                {
                    ResultToEdit.Name = model.Name;
                    ResultToEdit.OrganizationId = model.OrganizationId;
                    ent.SaveChanges();
                    return true;

                }
                else
                {
                    return false;
                }

            }
        }

        public bool IsAlreadyGroupName(int OrgId, string GroupName)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var result = ent.DepartmentDetails.Where(x => x.OrganizationId == OrgId && x.Name.Contains(GroupName)).ToList();
                if (result.Count > 0)
                {
                    return false;
                }
                else
                    return true;
            }

        }

        public bool DeleteGroup(int GroupId)
        {
            using (MeetingAppMobileEntities ent = new MeetingAppMobileEntities())
            {
                var ItemToRemove = ent.DepartmentDetails.SingleOrDefault(x => x.DepartmentId == GroupId);
                if (ItemToRemove != null)
                {
                    ent.DepartmentDetails.Remove(ItemToRemove);
                    ent.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
        }
    }
}