﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;
using System.Net.Http;
using System.Net;

namespace MobileAppAndroid.Controllers
{
    public class DepartmentMainController : Controller
    {
        //
        // GET: /DepartmentMain/

        public ActionResult Index()
        {
            DepartmentDetailsModel model = new DepartmentDetailsModel();
            DepartmentDetailsProviders pro = new DepartmentDetailsProviders();
            model.DepartmentDetailsModelList = pro.GetDepartmentList();
            return View(model);
        }

        public JsonResult Delete(string id)
        {
            DepartmentDetailsModel model = new DepartmentDetailsModel();
            model.DepartmentId = Convert.ToInt32(id);
            DepartmentDetailsProviders pro = new DepartmentDetailsProviders();
            var Message = new List<object>();

            if (pro.DeleteGroup(model.DepartmentId))
            {

                Message.Add(new { Success = "1", Message = "Successfully Deleted" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

            else
            {
                Message.Add(new { Success = "0", Message = "Error Please Try Again." });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

        }



        public ActionResult Movies()
        {
            var movies = new List<object>();

            movies.Add(new { Title = "Ghostbusters", Genre = "Comedy", Year = 1984 });


            return Json(movies, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetMP()
        {
            DepartmentDetailsModel model = new DepartmentDetailsModel();
            DepartmentDetailsProviders pro = new DepartmentDetailsProviders();
            model.DepartmentDetailsModelList = pro.GetDepartmentList();
            return Json(model.DepartmentDetailsModelList, JsonRequestBehavior.AllowGet);

        }
    }
}
