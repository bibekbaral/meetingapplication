﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMatrix.WebData;
using MobileAppAndroid.Models;
using System.Web;
using MobileAppAndroid.Providers;

namespace MobileAppAndroid.Controllers
{
    public class LoginVerificationController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public HttpResponseMessage Get(string UserName, string Password,string DeviceId)
        {
            EmployeeRecordsProviders pro = new EmployeeRecordsProviders();
            LoginModel model = new LoginModel();
            model.UserName = UserName;
            model.Password = Password;
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                //add /insert in Employee Table

                using (UsersContext ent = new UsersContext())
                {

                    var data = ent.UserProfiles.Where(x => x.UserName == model.UserName).SingleOrDefault();
                    LoginVerificationViewModel up = new LoginVerificationViewModel();
                    up.IsUserValid = 1;
                    up.UserType = data.UserType;
                    up.OrganizationId = data.OrganizationId;
                    up.EmployeeId = data.EmployeeId;
                    pro.InsertEmployeeDeviceInfo(Convert.ToInt32(up.EmployeeId), DeviceId);

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, up);
                    return response;
                }

            }
            else
            {
                LoginVerificationViewModel up = new LoginVerificationViewModel();
                up.IsUserValid = 0;
                up.UserType = 0;
                up.OrganizationId = 0;
                up.EmployeeId = 0;
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, up);
                return response;
            }


        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {


        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {


        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {

        }
    }
}