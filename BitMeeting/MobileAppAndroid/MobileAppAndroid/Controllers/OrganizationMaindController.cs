﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;

namespace MobileAppAndroid.Controllers
{
    public class OrganizationMaindController : Controller
    {
        //
        // GET: /OrganizationMaind/


        public ActionResult Index()
        {
            OrganizationDetailsModel model = new OrganizationDetailsModel();
            return View();
        }

        public ActionResult RegisterOrganization()
        {
            OrganizationDetailsModel model = new OrganizationDetailsModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult RegisterOrganization(OrganizationDetailsModel model)
        {

            OrganizationSetupProviders pro = new OrganizationSetupProviders();
            if (pro.InsertOrganization(model))
            {
                TempData["SuccessMessage"] = "success";
                return RedirectToAction("Success");
            }
            else
            {
                TempData["SuccessMessage"] = "Failure";
                return View(model);

            }
            
        }
        public ActionResult Success()
        {
            return View();
        }

    }
}
