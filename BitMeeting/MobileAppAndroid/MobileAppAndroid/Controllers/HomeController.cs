﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;

namespace MobileAppAndroid.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            HomeProviders pro = new HomeProviders();
            HomeViewModle model = new HomeViewModle();
            model.EmployeeDetailsVMHomeList = pro.GetEmployeeList();
            model.MeetingDetailsVMHomeList = pro.GetMeetingDetailsForHome();
            
            return View(model);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
