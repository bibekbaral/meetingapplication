﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;
using System.Web.Mvc;
using System.Text;
using Newtonsoft.Json.Linq;

namespace MobileAppAndroid.Controllers
{
    [RoutePrefix("api/{controller}/{action}")]
    public class DepartmentDetailsController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public HttpResponseMessage Get(int id)
        {
            DepartmentDetailsModel model = new DepartmentDetailsModel();
            DepartmentDetailsProviders pro = new DepartmentDetailsProviders();
            model.DepartmentDetailsModelList = pro.GetDepartmentList(id).Where(x => x.OrganizationId == id).ToList();
            List<DepartmentDetailsModel> ReturnList = new List<DepartmentDetailsModel>();
            //var result = new { Success = "True", Message = "Successfully Inserted" };
            foreach (var item in model.DepartmentDetailsModelList)
            {
                ReturnList.Add(new DepartmentDetailsModel { DepartmentId = item.DepartmentId, Name = item.Name });
            }
            return new HttpResponseMessage()
            {
                Content = new StringContent(JArray.FromObject(ReturnList).ToString(), Encoding.UTF8, "application/json")
                //Content=JsonConvert.SerializeObject(model.MeetingRecordsModelList);
            };
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {

        }


        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }


        // DELETE api/<controller>/5
        //[System.Web.Http.HttpDelete]
        //[System.Web.Http.ActionName("DeleteDept")]
        //public HttpResponseMessage DeleteDept(string id)
        //{

        //    DepartmentDetailsProviders pro = new DepartmentDetailsProviders();
        //    int DeptId = Convert.ToInt32(id);
        //    if (pro.DeleteGroup(DeptId))
        //    {
        //        var result = new { Success = "True", Message = "Successfully Deleted" };
        //        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        //        return response;
        //    }
        //    else
        //    {
        //        var result = new { Success = "False", Message = "Please Try Again" };
        //        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        //        return response;
        //    }
        //}



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]

        public HttpResponseMessage SaveDept(string id, string id2)
        {
            DepartmentDetailsModel model = new DepartmentDetailsModel();
            DepartmentDetailsProviders pro = new DepartmentDetailsProviders();
            model.OrganizationId = Convert.ToInt32(id);
            model.Name = id2;
            //Check Is alredy same group or not
            if (pro.IsAlreadyGroupName(model.OrganizationId, model.Name))
            {
                if (pro.InsertDepartment(model))
                {
                    var result = new { Success = "True", Message = "Successfully Inserted" };
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                    return response;
                }
                else
                {
                    var result = new { Success = "False", Message = "Error Message" };
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                    return response;

                }
            }
            else
            {
                var result = new { Success = "False", Message = "Same name was already inserted." };
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            }

        }
        [System.Web.Http.AcceptVerbs("EditDept")]
        [System.Web.Http.HttpGet]

        public HttpResponseMessage EditDept(string id, string id2, string id3)
        {
            DepartmentDetailsModel model = new DepartmentDetailsModel();
            DepartmentDetailsProviders pro = new DepartmentDetailsProviders();
            model.OrganizationId = Convert.ToInt32(id);
            model.Name = id2;
            model.DepartmentId = Convert.ToInt32(id3);
            if (pro.EditDeaprtment(model))
            {
                var result = new { Success = "True", Message = "Successfully Edited" };
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            }
            else
            {
                var result = new { Success = "False", Message = "Error Message" };
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                return response;

            }

        }



    }
}