﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;

namespace MobileAppAndroid.Controllers
{
    public class EmployeeMainController : Controller
    {
        //
        // GET: /EmployeeMain/

        EmployeeRecordsProviders pro = new EmployeeRecordsProviders();
        public ActionResult Index()
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            model.EmployeeDetailsModelList = pro.GetList();
            return View(model);
        }


        public ActionResult EditEmployee(string EmpId, string Name, string Email, string Password, string UserType, string OrganizationID, string DepartmentId)
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            EmployeeRecordsProviders pro = new EmployeeRecordsProviders();
            model.Name = Name;
            model.UserPassword = Password;
            model.Email = Email;
            model.OrganizationId = Convert.ToInt32(OrganizationID);
            model.DepartmentId = Convert.ToInt32(DepartmentId);
            model.UserType = Convert.ToInt32(UserType);

            //Check Is alredy same email or not


            var Message = new List<object>();

            if (pro.EditEmployee(model))
            {

                Message.Add(new { Success = "1", Message = "Successfully Edited" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

            else
            {
                Message.Add(new { Success = "0", Message = "Error Please Try Again." });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }


        }

        public ActionResult EditEmployeeName(string EmpId, string Name)
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            EmployeeRecordsProviders pro = new EmployeeRecordsProviders();
            model.Name = Name;
            model.EmployeeId = Convert.ToInt32(EmpId);

            //Check Is alredy same email or not

            var Message = new List<object>();

            if (pro.EditEmployeeName(model))
            {

                Message.Add(new { Success = "1", Message = "Successfully Edited" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

            else
            {
                Message.Add(new { Success = "0", Message = "Error Please Try Again." });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }


        }

        public JsonResult GetEmployeeForEditMeeting(string MeetingId)
        {
            int MeetingIdInt = Convert.ToInt32(MeetingId);
            //get OrganizationID from meeting id

            MeetingDetailsProviders pro1 = new MeetingDetailsProviders();
            int OrganizationId = pro1.GetOrganizationIdFromMeetingId(MeetingIdInt);
            List<EmployeeDetailsForMeetingEditViewModel> List = new List<EmployeeDetailsForMeetingEditViewModel>();
            if (OrganizationId > 0)
            {
                var EmployeeDetails = pro.GetList().Where(x => x.OrganizationId == OrganizationId);
                foreach (var item in EmployeeDetails)
                {
                    var Obj = new EmployeeDetailsForMeetingEditViewModel()
                    {

                        EmployeeName = item.Name,
                        CheckedStatus = Utilities.GetCheckedStatus(item.EmployeeId, MeetingIdInt),
                        EmployeeId = item.EmployeeId.ToString(),
                        MeetingId = MeetingId

                    };
                    List.Add(Obj);

                }
                return Json(List, JsonRequestBehavior.AllowGet);

            }
            else
            {
                var Message = new List<object>();
                Message.Add(new { Success = "0", Message = "Error Please Try Again." });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

        }


        public JsonResult DeleteEmployee(string EmployeeId)
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            model.EmployeeId = Convert.ToInt32(EmployeeId);
            //First delete employee from Meeting
            var Message = new List<object>();
            if (pro.DeleteEmployee(model.EmployeeId))
            {

                Message.Add(new { Success = "1", Message = "Delete Successfully." });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Message.Add(new { Success = "0", Message = "Error Please Try Again." });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
        }




    }
}
