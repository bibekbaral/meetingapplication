﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;
using System.Web.Mvc;
using System.Text;
using Newtonsoft.Json.Linq;

namespace MobileAppAndroid.Controllers
{
    public class MeetingDetailsController : ApiController
    {
        // GET api/<controller>

        List<MeetingDetailsMainModel> ListMeeting = new List<MeetingDetailsMainModel>();

        public IEnumerable<MeetingDetailsMainModel> Get()
        {
            MeetingDetailsProviders pro = new MeetingDetailsProviders();
            ListMeeting = pro.GetMeetingList();
            return ListMeeting;
        }


        // GET api/<controller>/5
        public HttpResponseMessage Get(int id)
        {
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            MeetingDetailsProviders pro = new MeetingDetailsProviders();
            model.MeetingDetailsMainModelList = pro.GetMeetingList(id).Where(x => x.OrganizationId == id).ToList();
            List<ReturnMeetingDetailsAdminViewModel> ReturnList = new List<ReturnMeetingDetailsAdminViewModel>();
            //var result = new { Success = "True", Message = "Successfully Inserted" };
            if (model.MeetingDetailsMainModelList.Count > 0)
            {
                foreach (var item in model.MeetingDetailsMainModelList)
                {
                    string MeetingIdStr = @"" + item.MeetiingDetailsId + "";
                    string OrganizationIdStr = @"" + id + "";

                    string TimeSpanDate = string.Format("{0:00}:{1:00}", item.MeetingTime.Hours, item.MeetingTime.Minutes);

                    ReturnList.Add(new ReturnMeetingDetailsAdminViewModel { MeetingDetailsId = MeetingIdStr, MeetingTitle = item.MeetingTitle.Replace("\"", string.Empty), Agenda = item.Agenda.Replace("\"", string.Empty), Venue = item.Venue.Replace("\"", string.Empty), MeetingTime = TimeSpanDate, MeetingDate = item.MeetingDate.ToShortDateString(), OrganizationId = OrganizationIdStr });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(ReturnList).ToString(), Encoding.UTF8, "application/json")
                    //Content=JsonConvert.SerializeObject(model.MeetingRecordsModelList);
                };
            }
            else
            {
                var message = string.Format("Product with id = {0} not found", id);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
        }



        // POST api/<controller>
        public void Post([FromBody]string value)
        {

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {



        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {

        }
    }
}