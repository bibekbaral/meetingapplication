﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;
using System.Globalization;
using ImageResizer;
using System.IO;

namespace MobileAppAndroid.Controllers
{
    public class MeetingDetailsMainController : Controller
    {

        // GET: /MeetingDetailsMain/
        MeetingDetailsProviders pro = new MeetingDetailsProviders();
        public ActionResult Index()
        {

            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            model.MeetingDetailsMainModelList = pro.GetMeetingList();
            return View(model);
        }



        public ActionResult Create()
        {
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            return View(model);
        }


        [HttpPost]
        public ActionResult Create(MeetingDetailsMainModel model)
        {
            pro.InsertMeetingDetails(model);
            return RedirectToAction("Index");
        }


        public JsonResult CreateMeeting(string MeetingTitle, string Venue, string Agenda, string CreatedBy, string MeetingDate, string MeetingTime, string OrganizationId)
        {

            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            model.MeetingTitle = MeetingTitle;
            model.Venue = Venue;
            model.Agenda = Agenda;
            model.CreatedBy = Convert.ToInt32(CreatedBy);
            DateTime NewDt = Convert.ToDateTime(MeetingDate);
            model.MeetingDate = NewDt;
            //Check Date
            DateTime TodayDate = DateTime.Now;
            var Message = new List<object>();
            if (NewDt >= TodayDate)
            {

                model.MeetingTime = TimeSpan.Parse(MeetingTime);
                model.OrganizationId = Convert.ToInt32(OrganizationId);

                int MeetingId = pro.InsertMeetingDetails(model);
                string MeetingIdstr = @"" + MeetingId + "";
                if (MeetingId > 0)
                {
                    //Get all employee List

                    Message.Add(new { Success = "1", MeetingId = MeetingIdstr, Message = "Successfully Inserted" });
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Message.Add(new { Success = "0", MeetingId = "0", Message = "Error Please Try Again." });
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
            }

            else
            {
                Message.Add(new { Success = "0", MeetingId = "0", Message = "Date not valid." });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult EditMeetingCreateMeeting(string MeetingId, string MeetingTitle, string Venue, string Agenda, string CreatedBy, string MeetingDate, string MeetingTime, string OrganizationId)
        {
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            model.MeetingTitle = MeetingTitle;
            model.Venue = Venue;
            model.Agenda = Agenda;
            model.MeetiingDetailsId = Convert.ToInt32(MeetingId);
            model.CreatedBy = Convert.ToInt32(CreatedBy);
            DateTime NewDt = Convert.ToDateTime(MeetingDate);
            model.MeetingDate = NewDt;
            //Check Date
            DateTime TodayDate = DateTime.Now;
            var Message = new List<object>();

            if (NewDt >= TodayDate)
            {
                model.MeetingTime = TimeSpan.Parse(MeetingTime);
                model.OrganizationId = Convert.ToInt32(OrganizationId);

                if (pro.EditMeetingDetails(model))
                {
                    string MeetingIdstrForVal = @"" + MeetingId + "";
                    Message.Add(new { Success = "1", MeetingId = MeetingIdstrForVal, Message = "Successfully Edited" });
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string MeetingIdstrForVal = @"" + MeetingId + "";
                    Message.Add(new { Success = "0", MeetingId = "0", Message = "No changes made." });
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Message.Add(new { Success = "0", MeetingId = "0", Message = "Date not valid." });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteMeeting(string MeetingId)
        {
            var Message = new List<object>();

            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            model.ObjMeetingEmployeeDetailsViewModel.MeetingId = Convert.ToInt32(MeetingId);

            if (pro.DeleteMeetingDetails(model.ObjMeetingEmployeeDetailsViewModel))
            {
                Message.Add(new { Success = "1", Message = "Successfully Deleted" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Message.Add(new { Success = "0", Message = "Please Try Again" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
        }




        public JsonResult MeetingEmployeeDetails(string MeetingId, [System.Web.Http.FromUri] string employeeId)
        {
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            String[] EmployeeIds = employeeId.Split(',');
            model.ObjMeetingEmployeeDetailsViewModel.MeetingId = Convert.ToInt32(MeetingId);
            model.ObjMeetingEmployeeDetailsViewModel.Status = 1;
            model.ObjMeetingEmployeeDetailsViewModel.Remarks = "";
            model.ObjMeetingEmployeeDetailsViewModel.Feedback = "";

            var Message = new List<object>();

            if (pro.InsertEmployeeInMeeting(model, EmployeeIds))
            {
                //Send Notification.........................................
                pro.SendNotificationToUser(model.ObjMeetingEmployeeDetailsViewModel.MeetingId);
                Message.Add(new { Success = "1", Message = "Successfully Inserted" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Message.Add(new { Success = "0", Message = "Error Please Try Again" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

        }




        public JsonResult MeetingEmpGroupDetails(string MeetingId, [System.Web.Http.FromUri] string GroupId)
        {
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            String[] GroupIds = GroupId.Split(',');
            model.ObjMeetingEmployeeDetailsViewModel.MeetingId = Convert.ToInt32(MeetingId);
            model.ObjMeetingEmployeeDetailsViewModel.Status = 1;
            model.ObjMeetingEmployeeDetailsViewModel.Remarks = "";
            model.ObjMeetingEmployeeDetailsViewModel.Feedback = "";

            var Message = new List<object>();
            model.EmployeeFromGroupIdViewModelList = pro.GetEmployeeListFromGroupIds(GroupIds);
            if (pro.InsertEmployeeGroupInMeeting(model))
            {
                pro.SendNotificationToUser(model.ObjMeetingEmployeeDetailsViewModel.MeetingId);
                Message.Add(new { Success = "1", Message = "Successfully Inserted" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

            else
            {
                Message.Add(new { Success = "0", Message = "Error Please Try Again" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

        }




        //List Meeting For Employee
        public JsonResult GetMeetingListForEmp(string OrganizationId, string EmployeeId)
        {

            int OrgId = Convert.ToInt32(OrganizationId);
            int EmpId = Convert.ToInt32(EmployeeId);
            EmployeeDetailsModel model = new EmployeeDetailsModel();

            model.EmployeeMeetingDetailViewModelList = pro.GetEmployeeMeetingDetails(OrgId, EmpId).ToList();
            var Message = new List<object>();
            List<ReturnEmployeeMeetingDetailViewModel> ReturnList = new List<ReturnEmployeeMeetingDetailViewModel>();

            if (model.EmployeeMeetingDetailViewModelList.Count > 0)
            {
                foreach (var item in model.EmployeeMeetingDetailViewModelList)
                {
                    string MeetingIdStr = @"" + item.MeetiingDetailsId + "";
                    string OrganizationIdStr = @"" + OrganizationId + "";
                    string YesNoStatusStr = @"" + item.Status + "";

                    string TimeSpanDate = string.Format("{0:00}:{1:00}", item.MeetingTime.Hours, item.MeetingTime.Minutes);

                    if (string.IsNullOrEmpty(item.Remarks))
                    {
                        item.Remarks = @"" + 0 + "";
                    }
                    if (string.IsNullOrEmpty(item.FeedBack))
                    {
                        item.FeedBack = @"" + 0 + "";
                    }

                    ReturnList.Add(new ReturnEmployeeMeetingDetailViewModel { MeetiingDetailsId = MeetingIdStr, MeetingTitle = item.MeetingTitle.Replace("\"", string.Empty), Agenda = item.Agenda.Replace("\"", string.Empty), Venue = item.Venue.Replace("\"", string.Empty), MeetingTime = TimeSpanDate, MeetingDate = item.MeetingDate.ToShortDateString(), OrganizationId = OrganizationIdStr, YesNoStatus = YesNoStatusStr, Remarks = item.Remarks.Replace("\"", string.Empty), FeedBack = item.FeedBack.Replace("\"", string.Empty) });
                }
                //Message.Add(new { Success = "0", Message = "Error Please Try Again" });
                return Json(ReturnList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //Message.Add(new { Success = "0", Message = "Error Please Try Again" });
                return Json(ReturnList, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetEmployeeListFromMeetingId(string MeetingId)
        {


            int MeetingIdInt = Convert.ToInt32(MeetingId);
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            model.EmployeeMeetingDetailViewModelList = pro.GetEmployeeListFromMeetingId(MeetingIdInt).ToList();
            var Message = new List<object>();
            Message.Add(new { Success = "0", Message = "Error Please Try Again" });
            return Json(model.EmployeeMeetingDetailViewModelList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCities(string query)
        {
            var data = new
            {
                query = "Unit",
                suggestions = new[]
    {
      new { value = "United Arab Emirates", data = "AE" },
      new { value = "United Kingdom", data = "UK" },
      new { value = "United States", data = "US" }
    }
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMeetingListForEmployee(string OrganizationId, string EmployeeId)
        {

            int OrgId = Convert.ToInt32(OrganizationId);
            int EmpId = Convert.ToInt32(EmployeeId);
            EmployeeDetailsModel model = new EmployeeDetailsModel();

            model.EmployeeMeetingDetailViewModelList = pro.GetEmployeeMeetingDetails(OrgId, EmpId).ToList();
            var Message = new List<object>();
            List<ReturnEmployeeMeetingDetailViewModel> ReturnList = new List<ReturnEmployeeMeetingDetailViewModel>();

            if (model.EmployeeMeetingDetailViewModelList.Count > 0)
            {
                foreach (var item in model.EmployeeMeetingDetailViewModelList)
                {
                    string MeetingIdStr = @"" + item.MeetiingDetailsId + "";
                    string OrganizationIdStr = @"" + OrganizationId + "";

                    string TimeSpanDate = string.Format("{0:00}:{1:00}", item.MeetingTime.Hours, item.MeetingTime.Minutes);

                    ReturnList.Add(new ReturnEmployeeMeetingDetailViewModel { MeetingTitle = item.MeetingTitle.Replace("\"", string.Empty), Agenda = item.Agenda.Replace("\"", string.Empty), Venue = item.Venue.Replace("\"", string.Empty), MeetingTime = TimeSpanDate, MeetingDate = item.MeetingDate.ToShortDateString(), OrganizationId = OrganizationIdStr });
                }

                return Json(ReturnList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(ReturnList, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult YesNoStatus(string MeetingId, string EmployeeId, string YesNoStatus, string Remarks)
        {
            var Message = new List<object>();

            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            model.ObjMeetingEmployeeDetailsViewModel.MeetingId = Convert.ToInt32(MeetingId);
            model.ObjMeetingEmployeeDetailsViewModel.EmployeeId = Convert.ToInt32(EmployeeId);
            model.ObjMeetingEmployeeDetailsViewModel.Status = Convert.ToInt32(YesNoStatus);
            if (string.IsNullOrEmpty(Remarks))
            {
                model.ObjMeetingEmployeeDetailsViewModel.Remarks = "";
            }
            else
            {
                model.ObjMeetingEmployeeDetailsViewModel.Remarks = Remarks;
            }

            if (pro.UpdateYesNoStaus(model))
            {
                Message.Add(new { Success = "1", Message = "Successfully Inserted" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Message.Add(new { Success = "0", Message = "Error Please Try Again" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult AddFeedback(string MeetingId, string EmployeeId, string Feedback)
        {
            var Message = new List<object>();
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            model.ObjMeetingEmployeeDetailsViewModel.MeetingId = Convert.ToInt32(MeetingId);
            model.ObjMeetingEmployeeDetailsViewModel.EmployeeId = Convert.ToInt32(EmployeeId);
            if (string.IsNullOrEmpty(Feedback))
            {
                model.ObjMeetingEmployeeDetailsViewModel.Feedback = "";
            }
            else
            {
                model.ObjMeetingEmployeeDetailsViewModel.Feedback = Feedback;
            }

            if (pro.InsertFeedback(model))
            {
                Message.Add(new { Success = "1", Message = "Successfully Inserted" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Message.Add(new { Success = "0", Message = "Error Please Try Again" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetMeetingDetailsForAdmin(string MeetingId)
        {
            var Message = new List<object>();
            if (string.IsNullOrEmpty(MeetingId))
            {
                Message.Add(new { Success = "0", Message = "Error Please Try Again" });
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                int MeetingIdInt = Convert.ToInt32(MeetingId);
                MeetingDetailsMainModel model = new MeetingDetailsMainModel();
                model.MeetingEmployeeDetailsForAdminVMList = pro.GetMeetingDetailsforAdmin(MeetingIdInt).ToList();
                List<MeetingEmployeeDetailsForAdminVM> ReturnList = new List<MeetingEmployeeDetailsForAdminVM>();


                if (model.MeetingEmployeeDetailsForAdminVMList.Count > 0)
                {
                    int totalAttendee = pro.GetTotalAttendeeNumber(MeetingIdInt);

                    foreach (var item in model.MeetingEmployeeDetailsForAdminVMList)
                    {

                        //string TimeSpanDate = string.Format("{0:00}:{1:00}", item.MeetingTime.Hours, item.MeetingTime.Minutes);
                        string YesNoStatusStr = pro.GetYesNoStaus(Convert.ToInt32(item.YesNoStatus));
                        ReturnList.Add(new MeetingEmployeeDetailsForAdminVM { EmployeeName = item.EmployeeName.Replace("\"", string.Empty), Remarks = item.Remarks.Replace("\"", string.Empty), FeedBack = item.FeedBack.Replace("\"", string.Empty), YesNoStatus = YesNoStatusStr, TotalAttendee = totalAttendee.ToString() });
                    }

                    return Json(ReturnList, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(ReturnList, JsonRequestBehavior.AllowGet);
                }
            }


        }

        public ActionResult InsertImageTest()
        {
            return View();
        }

        [HttpPost]
        public JsonResult InsertImagea(HttpPostedFileBase file)
        {

            var Message = new List<object>();


            try
            {
                if (file.ContentLength > 0)
                {
                    Message.Add(new { Success = "1", Message = "Successfully Inserted." });
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Upp/Images"), "HariKumar");
                    Message.Add(new { Success = "1", Message = path });
                    file.SaveAs(path);
                }

            }
            catch
            {
                var fileNamea = Path.GetFileName(file.FileName);
                var patha = Path.Combine(Server.MapPath("~/Upp/Images"), "RamChandra");
                Message.Add(new { Success = "1", Message = patha });
            }

            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsertImage([System.Web.Http.FromBody] string imageFile)
        {
            var Message = new List<object>();


            string imageDataParsed = imageFile.Substring(imageFile.IndexOf(',') + 1);

            byte[] imageBytes = Convert.FromBase64String(imageDataParsed);
            var imageStream = new MemoryStream(imageBytes, false);
            Message.Add(new { Success = "1", Message = imageStream });





            //try
            //{
            //    if (file.ContentLength > 0)
            //    {
            //        Message.Add(new { Success = "1", Message = "Successfully Inserted." });
            //        var fileName = Path.GetFileName(file.FileName);
            //        var path = Path.Combine(Server.MapPath("~/Upp/Images"), "HariKumar");
            //        Message.Add(new { Success = "1", Message = path });
            //        file.SaveAs(path);
            //    }

            //}
            //catch
            //{
            //    var fileNamea = Path.GetFileName(file.FileName);
            //    var patha = Path.Combine(Server.MapPath("~/Upp/Images"), "RamChandra");
            //    Message.Add(new { Success = "1", Message = patha });
            //}

            return Json(Message, JsonRequestBehavior.AllowGet);
        }








    }
}
