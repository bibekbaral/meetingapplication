﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;
using System.Text;
using Newtonsoft.Json.Linq;

namespace MobileAppAndroid.Controllers
{
    [RoutePrefix("api/{controller}/{action}")]
    public class EmployeeAPIController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<EmployeeDetailsModel> Get()
        {
            EmployeeRecordsProviders pro = new EmployeeRecordsProviders();
            List<EmployeeDetailsModel> ReturnList = new List<EmployeeDetailsModel>();
            ReturnList = pro.GetList();
            return ReturnList;
        }

        // GET api/<controller>/5
        public HttpResponseMessage Get(int id)
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            EmployeeRecordsProviders pro = new EmployeeRecordsProviders();
            model.EmployeeDetailsModelList = pro.GetList(id).Where(x => x.OrganizationId == id).ToList();
            List<ReturnEmployeeModel> ReturnList = new List<ReturnEmployeeModel>();
            //var result = new { Success = "True", Message = "Successfully Inserted" };
            foreach (var item in model.EmployeeDetailsModelList)
            {
                string EmployeeId = @"" + item.EmployeeId + "";
                string DepartMentId = @"" + item.DepartmentId + "";
                string UserTypeId = @"" + item.UserType + "";
                string OrganizationIdStr = @"" + item.OrganizationId + "";

                ReturnList.Add(new ReturnEmployeeModel { EmployeeIdStr = EmployeeId, DepartmentIdStr = DepartMentId, Name = item.Name, UserTypeStr = UserTypeId, Email = item.Email, OrganizationIdStr = OrganizationIdStr });
            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(JArray.FromObject(ReturnList).ToString(), Encoding.UTF8, "application/json")
                //Content=JsonConvert.SerializeObject(model.MeetingRecordsModelList);
            };
        }



        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]

        public HttpResponseMessage SaveEmployee(string Name, string Email, string Password, string UserType, string OrganizationID, string DepartmentId)
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            EmployeeRecordsProviders pro = new EmployeeRecordsProviders();
            model.Name = Name;
            model.UserPassword = Password;
            model.Email = Email;
            model.OrganizationId = Convert.ToInt32(OrganizationID);
            model.DepartmentId = Convert.ToInt32(DepartmentId);
            model.UserType = Convert.ToInt32(UserType);

            //Check Is alredy same email or not

            if (pro.IsAlreadyemail(model.Email))
            {
                if (pro.InsertEmployee(model))
                {
                    var result = new { Success = "True", Message = "Successfully Inserted" };
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                    return response;
                }
                else
                {
                    var result = new { Success = "False", Message = "Error Message" };
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
                    return response;

                }
            }
            else
            {
                var resultFail = new { Success = "False", Message = "Same Email is already in our system." };
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, resultFail);
                return response;
            }

        }



    }
}