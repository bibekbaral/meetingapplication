﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAppAndroid.Areas.Employee.Models
{
    public class EmployeeHomeModel
    {

    }

    public class UpcomingMeetingViewModel
    {
        public int MeetingId { get; set; }
        public string MeetingTitle { get; set; }
        public string MeetingAgenda { get; set; }
        public string Venue { get; set; }

    }
}