﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;

namespace MobileAppAndroid.Areas.Employee.Controllers
{
    [Authorize]
    public class UpcomingMeetingForEmployeeController : Controller
    {
        //
        // GET: /Employee/UpcomingMeetingForEmployee/

        public ActionResult Index()
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            MeetingDetailsProviders pro = new MeetingDetailsProviders();
            int EmployeeId = Utilities.GetEmployeeIdFromUserId();
            int OrganizationId = Utilities.GetCurrentEmployeeOrganizationId(EmployeeId);
            DateTime TodayDate = DateTime.Today;

            model.EmployeeMeetingDetailViewModelList = pro.GetEmployeeMeetingDetails(OrganizationId, EmployeeId).Where(x => x.MeetingDate >= TodayDate).ToList();
            return View(model);
        }

        public ActionResult ViewMeetingDetails(int id)
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            MeetingDetailsProviders pro = new MeetingDetailsProviders();
            int EmployeeId = Utilities.GetEmployeeIdFromUserId();
            int OrganizationId = Utilities.GetCurrentEmployeeOrganizationId(EmployeeId);
            DateTime TodayDate = DateTime.Today;

            model.ObjEmployeeMeetingDetailViewModel = pro.GetEmployeeMeetingDetails(OrganizationId, EmployeeId).Where(x=>x.MeetiingDetailsId == id).SingleOrDefault();
            //model.ObjEmployeeMeetingDetailViewModel
            return View(model);
        }

        public ActionResult ListMeeting()
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            MeetingDetailsProviders pro = new MeetingDetailsProviders();
            int EmployeeId = Utilities.GetEmployeeIdFromUserId();
            int OrganizationId = Utilities.GetCurrentEmployeeOrganizationId(EmployeeId);
            model.EmployeeMeetingDetailViewModelList = pro.GetEmployeeMeetingDetails(OrganizationId, EmployeeId).ToList();
            return View(model);
        }
        public ActionResult PostFeedback(int id, int id2)
        {
            return View();
        }

        public ActionResult PostRemarks(int id, int id2)
        {
            return View();
        }


    }
}
