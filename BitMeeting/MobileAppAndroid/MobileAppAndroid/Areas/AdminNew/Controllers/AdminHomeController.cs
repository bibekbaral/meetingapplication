﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;
using MobileAppAndroid.Areas.AdminNew.Models;

namespace MobileAppAndroid.Areas.AdminNew.Controllers
{
    public class AdminHomeController : Controller
    {
        //
        // GET: /Admin/AdminHome/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }
        public ActionResult ShowHome()
        {
            return View();
        }
        public ActionResult ShowCalendar()
        {

            return View();
        }

        public JsonResult ListMeetingForCalendar()
        {
            List<CalendarViewModel> ReturnList = new List<CalendarViewModel>();
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            MeetingDetailsProviders pro = new MeetingDetailsProviders();
            model.MeetingDetailsMainModelList = pro.GetMeetingList(1);
            foreach (var item in model.MeetingDetailsMainModelList)
            {
                string AllDays="False";
                ReturnList.Add(new CalendarViewModel { title = item.MeetingTitle, start = item.MeetingDate.ToShortDateString(), end = item.MeetingDate.ToShortDateString(), allDay = AllDays });
            }

            return Json(ReturnList, JsonRequestBehavior.AllowGet);

        }

    }
}
