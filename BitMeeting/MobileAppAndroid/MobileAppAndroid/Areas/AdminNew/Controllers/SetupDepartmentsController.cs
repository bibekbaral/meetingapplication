﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;

namespace MobileAppAndroid.Areas.AdminNew.Controllers
{
    public class SetupDepartmentsController : Controller
    {
        //
        // GET: /Admin/SetupDepartments/
        DepartmentDetailsProviders pro = new DepartmentDetailsProviders();
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index()
        {
            DepartmentDetailsModel model = new DepartmentDetailsModel();
            model.DepartmentDetailsModelList = pro.GetDepartmentList();
            return View(model);
        }

        public ActionResult Create()
        {
            DepartmentDetailsModel model = new DepartmentDetailsModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(DepartmentDetailsModel model)
        {
            if (pro.InsertDepartment(model))
            {
                TempData["SuccessMessage"] = "success";
            }
            else
            {
                TempData["SuccessMessage"] = "Failure";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            DepartmentDetailsModel model = new DepartmentDetailsModel();
            model = pro.GetDepartmentList().Where(x => x.DepartmentId == id).SingleOrDefault();
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(DepartmentDetailsModel model)
        {

            if (pro.EditDeaprtment(model))
            {
                TempData["SuccessMessage"] = "success";
            }
            else
            {
                TempData["SuccessMessage"] = "Failure";
            }

            return RedirectToAction("Index");
        }


    }


}
