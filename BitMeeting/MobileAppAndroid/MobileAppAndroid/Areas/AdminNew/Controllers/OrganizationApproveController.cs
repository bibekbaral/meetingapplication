﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;

namespace MobileAppAndroid.Areas.AdminNew.Controllers
{
    public class OrganizationApproveController : Controller
    {
        //
        // GET: /AdminNew/OrganizationApprove/
        OrganizationSetupProviders pro = new OrganizationSetupProviders();
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index()
        {
            //list organization details
            OrganizationDetailsModel model = new OrganizationDetailsModel();
            model.OrganizationDetailsModelList = pro.GetList();
            return View(model);
        }

        public ActionResult ApproveOrRejection(int id, int id2)
        {
            OrganizationDetailsModel model = new OrganizationDetailsModel();
            //0-delete, 1-request, 2-approved, 3-Rejected
            //Get Organization Status
            if (pro.ApproveOrRejectStatus(id, id2))
            {
                TempData["SuccessMessage"] = "success";
            }
            else
                TempData["SuccessMessage"] = "success";

            return RedirectToAction("Index");



        }

        public ActionResult Create()
        {
            OrganizationDetailsModel model = new OrganizationDetailsModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(OrganizationDetailsModel model)
        {

            //0-delete, 1-request, 2-approved, 3-Rejected
            //Get Organization Status
            if (pro.InsertOrganization(model))
            {
                TempData["SuccessMessage"] = "success";
            }
            else
                TempData["SuccessMessage"] = "success";

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            OrganizationDetailsModel model = new OrganizationDetailsModel();
            model = pro.GetList().Where(x => x.Id == id).SingleOrDefault();
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(OrganizationDetailsModel model)
        {
            if (pro.Edit(model))
            {
                TempData["SuccessMessage"] = "success";
            }
            else
                TempData["SuccessMessage"] = "success";

            return RedirectToAction("Index");
        }


    }
}
