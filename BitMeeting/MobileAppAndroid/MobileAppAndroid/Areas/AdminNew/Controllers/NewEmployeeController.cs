﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;

namespace MobileAppAndroid.Areas.AdminNew.Controllers
{
    public class NewEmployeeController : Controller
    {
        //
        // GET: /Admin/NewEmployee/
        EmployeeRecordsProviders pro = new EmployeeRecordsProviders();
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index()
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            model.EmployeeDetailsModelList = pro.GetList(1);
            return View(model);
        }

        public ActionResult Create()
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(EmployeeDetailsModel model)
        {
            if (pro.InsertEmployee(model))
            {
                TempData["SuccessMessage"] = "success";
            }
            else
            {
                TempData["SuccessMessage"] = "Failure";
            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            model = pro.GetList().Where(x => x.EmployeeId == id).SingleOrDefault();
            return RedirectToAction("Index");
        }


    }
}
