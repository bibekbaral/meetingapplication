﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileAppAndroid.Models;
using MobileAppAndroid.Providers;
using System.Net;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;


namespace MobileAppAndroid.Areas.AdminNew.Controllers
{
    public class AddNewMeetingController : Controller
    {
        //
        // GET: /Admin/AddNewMeeting/
        MeetingDetailsProviders pro = new MeetingDetailsProviders();
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index()
        {
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            model.MeetingDetailsMainModelList = pro.GetMeetingList();
            return View(model);
        }
        public ActionResult SendNotificationOld()
        {
            //AndroidGCMPushNotification mm = new AndroidGCMPushNotification();
            string ssapk = "AAAAmj_63s8:APA91bFLo3dHQwbrI_KNtRjvFr34__Mu8iByqYPuz-Edzc12cFWUzeH_xauk9wiDDPsYeJcHnmkLTdZys8ymPm9R_-EEl7sa-KepFqfUsEHj6jp3TE8rDMY4rMmDEczcfvNm8am86rOyQX-bWixXv_VLn4VXNjWEQw";
            var ssid = "662498369231";
            string deviceIdcc = "dm_g7DQrAjU:APA91bF6Jx8I4tJFYuO2DLTn30OByM8nh_RzkNvErgPRYfmOvUkZz-oEg-oldZW2ZT5HMETEFr_xUbIhmPI7fo-Kc7V6IjzNAFLaT2wDvwCQ9p3Wwr06GgLjhlqtiEnSlbalyuv1a3ju";
            string messagecc = "testing from code";

            //SendNotificationControllerMM(ssapk, ssid, deviceIdcc, messagecc);

            //mm.SendNotification("dm_g7DQrAjU:APA91bF6Jx8I4tJFYuO2DLTn30OByM8nh_RzkNvErgPRYfmOvUkZz-oEg-oldZW2ZT5HMETEFr_xUbIhmPI7fo-Kc7V6IjzNAFLaT2wDvwCQ9p3Wwr06GgLjhlqtiEnSlbalyuv1a3ju", "Hello Welcome");
            return View();
        }

        public ActionResult AssingEmployee(int id)
        {
            EmployeeDetailsModel model = new EmployeeDetailsModel();
            model.ObjEmployeeMeetingDetailViewModel.MeetiingDetailsId = id;
            EmployeeRecordsProviders empPro=new EmployeeRecordsProviders();
            //Check is Already Assgin Or Not
            model.EmployeeCheckListVMList = empPro.EmployeeCheckList(1);
            if (pro.CheckIsAlreadyEmployeeAssignOrNot(id) > 0)
            {
                model.EmployeeCheckListVMList = empPro.EmployeeCheckListIfExist(id);
            }
           
           

            return View(model);
        
        }

        [HttpPost]
        public ActionResult AssingEmployee(EmployeeDetailsModel model)
        {
            EmployeeRecordsProviders proEmp = new EmployeeRecordsProviders();
            if (proEmp.AssignEmployeeInMeetingFromAdmin(model))
            {
                TempData["SuccessMessage"] = "Meeting Created Successfully";
            }
            else
            {
                TempData["SuccessMessage"] = "Failure";
            }
            return RedirectToAction("Index");
        }

        public ActionResult ViewAssignEmployee(int id)
        {
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            model.MeetingEmployeeDetailsForAdminVMList = pro.GetMeetingDetailsforAdmin(id).ToList();
            return View(model);
        }
        

        public ActionResult Create()
        {
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(MeetingDetailsMainModel model)
        {
            string MeetingTimeStr = model.MeetiingDetailsIdStr;

            model.MeetingTime = TimeSpan.Parse(model.MeetiingDetailsIdStr);
            model.MeetingDate = DateTime.Parse(model.ObjReturnMeetingDetailsAdminViewModel.MeetingDate);
            model.CreatedBy = 1;
            model.OrganizationId = 1;
            if (pro.InsertMeetingDetailsAdmin(model))
            {
                TempData["SuccessMessage"] = "Meeting Created Successfully";
            }
            else
            {
                TempData["SuccessMessage"] = "Failure";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            MeetingDetailsMainModel model = new MeetingDetailsMainModel();
            model = pro.GetMeetingList().Where(x => x.MeetiingDetailsId == id).SingleOrDefault();
            return View(model);
        }

        public ActionResult DeleteMeeting(int id)
        {
            MeetingEmployeeDetailsViewModel model = new MeetingEmployeeDetailsViewModel();
            model.MeetingId = id;
            if (pro.DeleteMeetingDetails(model))
            {
                TempData["SuccessMessage"] = "Meeting Deleted Successfully";
            }
            else
            {
                TempData["SuccessMessage"] = "Error Please Try Again";
            }
            return RedirectToAction("Index");
        }







    }
}
