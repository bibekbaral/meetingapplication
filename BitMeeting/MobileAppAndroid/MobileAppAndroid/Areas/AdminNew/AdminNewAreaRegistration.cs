﻿using System.Web.Mvc;

namespace MobileAppAndroid.Areas.AdminNew
{
    public class AdminNewAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AdminNew";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AdminNew_default",
                "AdminNew/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
