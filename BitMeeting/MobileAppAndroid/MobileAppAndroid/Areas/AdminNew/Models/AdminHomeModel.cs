﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAppAndroid.Areas.AdminNew.Models
{
    public class AdminHomeModel
    {


    }

    public class CalendarViewModel
    {
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string allDay { get; set; }

        public List<CalendarViewModel> CalendarViewModelList { get; set; }

    }
}