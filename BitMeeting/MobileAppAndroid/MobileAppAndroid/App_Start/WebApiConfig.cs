﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MobileAppAndroid
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional, id2 = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
        name: "LoginVerification",
        routeTemplate: "api/{controller}/{UserName}/{Password}"
        );

            config.Routes.MapHttpRoute(
    name: "DepartmentDetails",
    routeTemplate: "api/{controller}/{action}/{id}/{id2}",
    defaults: new { id = RouteParameter.Optional, id2 = RouteParameter.Optional }
);
            //            config.Routes.MapHttpRoute(
            //  name: "EmployeeAPIController",
            //  routeTemplate: "api/{controller}/{action}/{id}/{id2}/{id3}/{id4}/{id5}/{id6}",
            //  defaults: new { id = RouteParameter.Optional }
            //);

            //    config.Routes.MapHttpRoute(
            //name: "DepartmentDetails",
            //routeTemplate: "api/{controller}/{DeptName}/{OrgId}"
            //);
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

        }
    }
}
