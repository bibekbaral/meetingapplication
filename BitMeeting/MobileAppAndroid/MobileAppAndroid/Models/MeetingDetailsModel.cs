﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAppAndroid.Models
{
    public class MeetingDetailsModel
    {

        public int MeetiingDetailsId { get; set; }
        public string MeetingTitle { get; set; }
        public string Agenda { get; set; }
        public DateTime MeetingDate { get; set; }

    }
}