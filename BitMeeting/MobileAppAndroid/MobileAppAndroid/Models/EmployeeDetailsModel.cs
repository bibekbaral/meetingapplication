﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAppAndroid.Models
{
    public class EmployeeDetailsModel
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int OrganizationId { get; set; }
        public int DepartmentId { get; set; }
        public int UserType { get; set; }

        public string UserPassword { get; set; }



        //For String
        public string EmployeeIdStr { get; set; }
        public string OrganizationIdStr { get; set; }
        public string DepartmentIdStr { get; set; }
        public string UserTypeStr { get; set; }




        public List<EmployeeDetailsModel> EmployeeDetailsModelList { get; set; }
        public ReturnEmployeeModel ObjReturnEmployeeModel { get; set; }

        public List<ReturnEmployeeModel> ReturnEmployeeModelList { get; set; }

        public EmployeeMeetingDetailViewModel ObjEmployeeMeetingDetailViewModel { get; set; }
        public List<EmployeeMeetingDetailViewModel> EmployeeMeetingDetailViewModelList { get; set; }

        public ReturnEmployeeMeetingDetailViewModel ObjReturnEmployeeMeetingDetailViewModel { get; set; }
        public List<ReturnEmployeeMeetingDetailViewModel> ReturnEmployeeMeetingDetailViewModelList { get; set; }



        public List<EmployeeCheckListVM> EmployeeCheckListVMList { get; set; }

        public EmployeeDetailsModel()
        {
            ReturnEmployeeModelList = new List<ReturnEmployeeModel>();
            ObjReturnEmployeeModel = new ReturnEmployeeModel();



            ObjEmployeeMeetingDetailViewModel = new EmployeeMeetingDetailViewModel();
            ObjReturnEmployeeMeetingDetailViewModel = new ReturnEmployeeMeetingDetailViewModel();

            ReturnEmployeeMeetingDetailViewModelList = new List<ReturnEmployeeMeetingDetailViewModel>();
            EmployeeMeetingDetailViewModelList = new List<EmployeeMeetingDetailViewModel>();


            EmployeeCheckListVMList = new List<EmployeeCheckListVM>();
        }
    }


    public class ReturnEmployeeModel
    {
        //For String
        public string EmployeeIdStr { get; set; }
        public string OrganizationIdStr { get; set; }
        public string DepartmentIdStr { get; set; }
        public string UserTypeStr { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }


    public class EmployeeMeetingDetailViewModel
    {
        public int MeetiingDetailsId { get; set; }
        public string MeetingTitle { get; set; }
        public string Agenda { get; set; }
        public DateTime MeetingDate { get; set; }
        public TimeSpan MeetingTime { get; set; }
        public int OrganizationId { get; set; }
        public string Venue { get; set; }
        public int EmployeeId { get; set; }
        public string Remarks { get; set; }
        public string FeedBack { get; set; }
        public int Status { get; set; }


    }

    public class ReturnEmployeeMeetingDetailViewModel
    {
        public string MeetiingDetailsId { get; set; }
        public string MeetingTitle { get; set; }
        public string Agenda { get; set; }
        public string MeetingDate { get; set; }
        public string MeetingTime { get; set; }
        public string OrganizationId { get; set; }
        public string Venue { get; set; }
        //public int EmployeeId { get; set; }
        public string Remarks { get; set; }
        public string FeedBack { get; set; }
        public string YesNoStatus { get; set; }


    }

    public class EmployeeDetailsForMeetingEditViewModel
    {
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string MeetingId { get; set; }
        public string CheckedStatus { get; set; }
    }

    public class EmployeeCheckListVM
    {
        public int EmployeeId { get; set; }
        public bool IsChecked { get; set; }
        public string EmployeeName { get; set; }
    }
}