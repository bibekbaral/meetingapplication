﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAppAndroid.Models
{
    public class OrganizationDetailsModel
    {
        public int Id { get; set; }
        public string OrganizationName { get; set; }
        public string Address { get; set; }
        public string EmailId { get; set; }
        public string Mobile { get; set; }
        public string PhNumber { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }


        public List<OrganizationDetailsModel> OrganizationDetailsModelList { get; set; }
    }
}