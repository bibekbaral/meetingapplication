﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAppAndroid.Models
{
    public class HomeViewModle
    {

        public List<EmployeeDetailsVMHome> EmployeeDetailsVMHomeList { get; set; }
        public List<MeetingDetailsVMHome> MeetingDetailsVMHomeList { get; set; }

        public HomeViewModle()
        {
            EmployeeDetailsVMHomeList = new List<EmployeeDetailsVMHome>();
            MeetingDetailsVMHomeList = new List<MeetingDetailsVMHome>();
        }
    }

    public class EmployeeDetailsVMHome
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int OrganizationId { get; set; }
        public int DepartmentId { get; set; }
        public int UserType { get; set; }
        public string DeviceId { get; set; }

    }

    public class MeetingDetailsVMHome
    {
        public int MeetingId { get; set; }
        public string MeetingTitle { get; set; }
        public DateTime MeetingDate { get; set; }
    }
}