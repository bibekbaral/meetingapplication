﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAppAndroid.Models
{
    public class MeetingDetailsMainModel
    {

        public int MeetiingDetailsId { get; set; }
        public string MeetingTitle { get; set; }
        public string Agenda { get; set; }
        public DateTime MeetingDate { get; set; }
        public TimeSpan MeetingTime { get; set; }
        public int CreatedBy { get; set; }
        public int OrganizationId { get; set; }
        public string Venue { get; set; }

        public string MeetiingDetailsIdStr { get; set; }


        public List<MeetingDetailsMainModel> MeetingDetailsMainModelList { get; set; }
        public List<MeetingEmployeeDetailsViewModel> MeetingEmployeeDetailsViewModelList { get; set; }
        public MeetingEmployeeDetailsViewModel ObjMeetingEmployeeDetailsViewModel { get; set; }

        public ReturnMeetingDetailsAdminViewModel ObjReturnMeetingDetailsAdminViewModel { get; set; }
        public List<ReturnMeetingDetailsAdminViewModel> ReturnMeetingDetailsAdminViewModelList { get; set; }

        public List<EmployeeFromGroupIdViewModel> EmployeeFromGroupIdViewModelList { get; set; }
        public EmployeeFromGroupIdViewModel ObjEmployeeFromGroupIdViewModel { get; set; }

        public List<MeetingEmployeeDetailsForAdminVM> MeetingEmployeeDetailsForAdminVMList { get; set; }

        public MeetingDetailsMainModel()
        {
            ObjMeetingEmployeeDetailsViewModel = new MeetingEmployeeDetailsViewModel();
            MeetingEmployeeDetailsViewModelList = new List<MeetingEmployeeDetailsViewModel>();
            ReturnMeetingDetailsAdminViewModelList = new List<ReturnMeetingDetailsAdminViewModel>();
            ObjReturnMeetingDetailsAdminViewModel = new ReturnMeetingDetailsAdminViewModel();

            EmployeeFromGroupIdViewModelList = new List<EmployeeFromGroupIdViewModel>();
            MeetingEmployeeDetailsForAdminVMList = new List<MeetingEmployeeDetailsForAdminVM>();

        }

    }

    public class MeetingEmployeeDetailsViewModel
    {

        public int MeetingEmpId { get; set; }
        public int MeetingId { get; set; }
        public int EmployeeId { get; set; }
        public int Status { get; set; }
        public string Remarks { get; set; }
        public string Feedback { get; set; }

    }

    public class ReturnMeetingDetailsAdminViewModel
    {
        public string MeetingDetailsId { get; set; }
        public string MeetingTitle { get; set; }
        public string Agenda { get; set; }
        public string MeetingDate { get; set; }
        public string MeetingTime { get; set; }
        public string OrganizationId { get; set; }
        public string Venue { get; set; }

    }

    public class EmployeeFromGroupIdViewModel
    {
        public int EmployeeIds { get; set; }
    }


    public class MeetingEmployeeDetailsForAdminVM
    {
        public string EmployeeName { get; set; }
        public string YesNoStatus { get; set; }
        public string FeedBack { get; set; }
        public string Remarks { get; set; }
        public string TotalAttendee { get; set; }
    }
}