﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAppAndroid.Models
{
    public class DepartmentDetailsModel
    {

        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public int OrganizationId { get; set; }
        public int? Status { get; set; }

        public List<DepartmentDetailsModel> DepartmentDetailsModelList { get; set; }


    }
}